package selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GoogleMainPage extends PageObject {

    public GoogleMainPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "hplogo")
    public WebElement googleMainPageLogo;

    @FindBy(name = "q")
    public WebElement mainGoogleSearchInputField;

    @FindBy(name = "btnK")
    public WebElement googlePromptPanelSearchButton;

    public void provideSearchedText(String keyword) {
        waitUntilElementIsClickable(mainGoogleSearchInputField);
        mainGoogleSearchInputField.clear();
        mainGoogleSearchInputField.sendKeys(keyword);
    }

    public void clickGooglePromptPanelSearchButton() {
        waitUntilElementIsClickable(googlePromptPanelSearchButton);
        googlePromptPanelSearchButton.click();
    }

}
