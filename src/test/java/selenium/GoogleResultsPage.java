package selenium;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class GoogleResultsPage extends PageObject {

    public GoogleResultsPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//a[@href='https://www.o2.pl/']")
    public WebElement o2PageRedirectionLink;

    public void clickO2PageRedirectionLink() {
        waitUntilElementIsClickable(o2PageRedirectionLink);
        o2PageRedirectionLink.click();
    }
}
